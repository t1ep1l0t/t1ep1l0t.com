import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import {PrismaService} from "../prisma.service";
import {HashService} from "../hash.service";

@Module({
  controllers: [UserController],
  providers: [UserService, PrismaService, HashService]
})
export class UserModule {}
