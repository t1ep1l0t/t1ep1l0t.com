import { ApiProperty } from "@nestjs/swagger";

export class UserDto {
    @ApiProperty({
        type: String,
        description: "User username. Example: t1ep1l0t",
        required: true,
        uniqueItems: true
    })
    username: string;

    @ApiProperty({
        type: String,
        description: "User E-mail. Example: t1ep1l0t@offconcern.com",
        required: true,
        uniqueItems: true
    })
    email: string;

    @ApiProperty({
        type: String,
        description: "User password. Example: 12345qwerty",
        required: true,
        uniqueItems: false
    })
    password: string;

    @ApiProperty({
        type: String,
        description: "User avatar. Example: File",
        required: false,
        uniqueItems: false
    })
    avatar?: Blob;

    @ApiProperty({
        type: Boolean,
        description: "User Email confirmation. Example: true",
        required: true,
        default: false
    })
    verified: boolean;
}

export class CreatedUserDto {
    @ApiProperty({
        type: Number,
        description: "User ID. Example: 1",
        required: true,
    })
    id: number;

    @ApiProperty({
        type: Date,
        description: "User updated Date time. Example: Sun Sep 10 2023 21:26:02 GMT+0300 (Москва, стандартное время)",
        required: true,
    })
    updatedAt: Date;

    @ApiProperty({
        type: String,
        description: "User username. Example: t1ep1l0t",
        required: true,
        uniqueItems: true
    })
    username: string;

    @ApiProperty({
        type: String,
        description: "User E-mail. Example: t1ep1l0t@offconcern.com",
        required: true,
        uniqueItems: true
    })
    email: string;

    @ApiProperty({
        type: String,
        description: "User password. Example: 12345qwerty",
        required: true,
        uniqueItems: false
    })
    password: string;

    @ApiProperty({
        type: String,
        description: "User avatar. Example: File",
        required: false,
        uniqueItems: false
    })
    avatar?: Blob;

    @ApiProperty({
        type: Boolean,
        description: "User Email confirmation. Example: true",
        required: true,
        default: false
    })
    verified: boolean;

    @ApiProperty({
        type: String,
        description: "User Refresh token. Example: asdasd5a77$dad5asd44asd$dadkasd7&addja$adashh&da",
        required: false,
    })
    refresh_token?: string;
}