import {BadRequestException, HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {User} from "@prisma/client";
import {PrismaService} from "../prisma.service";
import {UserDto} from "./user.dto";
import {HashService} from "../hash.service";

@Injectable()
export class UserService {
    constructor(
        private readonly prisma: PrismaService,
        private readonly HashService: HashService
    ) {}

    async createUser (user: UserDto) :Promise<User | HttpException> {
        if (!user.email || !user.username || !user.password) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `Email or username or password not found.`
            }, HttpStatus.BAD_REQUEST);
        }

        const check_username = await this.prisma.user.findUnique({where: {username: user.username}});
        if(check_username) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `User with username: ${user.username} already registered..`
            }, HttpStatus.BAD_REQUEST);
        }

        const check_email = await this.prisma.user.findUnique({where: {email: user.email}});
        if(check_email) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `User with E-mail: ${user.email} already registered..`
            }, HttpStatus.BAD_REQUEST);
        }

        const hash_password :string = await this.HashService.hashPassword(user.password);

        return this.prisma.user.create({
            data: {
                username: user.username,
                email: user.email,
                password: hash_password,
                verified: false
            }
        });
    }
    async deleteUser (id :number) :Promise<User | null> {
        try {
            if (!id) {
                throw new HttpException({
                    status: HttpStatus.BAD_REQUEST,
                    error: `ID is not found.`
                }, HttpStatus.BAD_REQUEST);
            }

            return await this.prisma.user.delete({where: {id : id}});

        } catch (e) {
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: `User with ID: ${id} not found.`
            }, HttpStatus.NOT_FOUND, {
                cause: e
            });
        }
    }
}
