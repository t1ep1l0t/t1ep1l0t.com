import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    HttpCode,
    HttpException,
    HttpStatus, NotFoundException,
    Param,
    Post,
    Put
} from '@nestjs/common';
import {ApiBody, ApiParam, ApiResponse, ApiTags} from "@nestjs/swagger";
import {UserService} from "./user.service";
import { User } from "@prisma/client";
import {CreatedUserDto, UserDto} from "./user.dto";

@ApiTags('User')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @ApiResponse({
        type: BadRequestException,
        description: 'If Email or username already registered. || Email or username or password not found.',
        status: HttpStatus.BAD_REQUEST
    })
    @ApiResponse({
        type: CreatedUserDto,
        description: 'If User successfully created.',
        status: HttpStatus.OK
    })
    @ApiBody({
        type: UserDto,
        description: "User object!",
        required: true
    })
    @HttpCode(200)
    @Post('create')
    async createUser (
        @Body() user: UserDto
    ) :Promise<User | HttpException> {
        return this.userService.createUser(user)
    }

    @Put('update/:id')
    async updateUser () {

    }

    @ApiResponse({
        type: NotFoundException,
        description: 'If user with this ID not found.',
        status: HttpStatus.NOT_FOUND
    })
    @ApiResponse({
        type: BadRequestException,
        description: 'If no ID has been specified.',
        status: HttpStatus.BAD_REQUEST
    })
    @ApiResponse({
        type: CreatedUserDto,
        description: 'If user with this ID successful deleted.',
        status: HttpStatus.CREATED
    })
    @ApiParam({
        type: String,
        description: "User ID for delete from database",
        name: 'id',
        required: true
    })
    @HttpCode(201)
    @Delete('delete/:id')
    async deleteUser (
        @Param('id') id: string
    ) :Promise<User | null> {
        return this.userService.deleteUser(Number(id))
    }
}
