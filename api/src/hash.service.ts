import * as bcrypt from 'bcrypt';

export class HashService {

    async hashPassword (password: string) :Promise<string> {
        return bcrypt.hash(password, 5)
    }

    async comparePasswords (password: string, hashPassword: string) :Promise<boolean> {
        return bcrypt.compare(password, hashPassword)
    }
}